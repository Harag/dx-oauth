(in-package :dx-oauth)

(defvar +utf-8+ (flexi-streams:make-external-format :utf8 :eol-style :lf))

(defmethod normalize-uri ((uri string))
  (normalize-uri (puri:parse-uri uri)))

(defmethod normalize-uri ((uri puri:uri))
  (let ((*print-case* :downcase) ; verify that this works!!
        (scheme (puri:uri-scheme uri))
        (host (puri:uri-host uri))
        (port (puri:uri-port uri))
        (path (puri:uri-path uri)))
    (values
      (concatenate 'string
        (string-downcase (princ-to-string scheme))
        "://"
        (string-downcase host)
        (cond
          ((null port)
           "")
          ((and (eq scheme :http) (eql port 80))
           "")
          ((and (eq scheme :https) (eql port 443))
           "")
          (t
           (concatenate 'string ":" (princ-to-string port))))
        path))))

(defun url-encode (string &optional (external-format +utf-8+))
  "URL-encodes a string using the external format EXTERNAL-FORMAT."
  (with-output-to-string (s)
    (loop for c across string
          for index from 0
          do (cond ((or (char<= #\0 c #\9)
                        (char<= #\a c #\z)
                        (char<= #\A c #\Z)
                        (find c "-_.~" :test #'char=))
                     (write-char c s))
                   (t (loop for octet across (flexi-streams:string-to-octets string
                                                                             :start index
                                                                             :end (1+ index)
                                                                             :external-format external-format)
                            do (format s "%~2,'0x" octet)))))))

(defun build-query-thingy (parameters)
  (with-output-to-string (stream)
    (loop for separator = "" then "&"
          for (key value) in parameters
          do
          (format stream "~a~a=~a"
                  separator
                  (url-encode key)
                  (url-encode value)))))

(defun signature-base-string (&key uri
                                   (request-method "POST")
                                   parameters)
  (format nil "~:@(~a~)&~a&~a"
          request-method
          (url-encode (normalize-uri uri))
          (url-encode (build-query-thingy
                       parameters))))

(defun hmac-key (consumer-secret &optional token-secret)
  (concatenate 'string (url-encode consumer-secret) "&" (url-encode (or token-secret ""))))

(defun hmac-sha1 (s key)
  (let* ((s (babel:string-to-octets s))
         (key (babel:string-to-octets key))
         (hmac (ironclad:make-hmac key 'ironclad:sha1)))
    (ironclad:update-hmac hmac s)
    (ironclad:hmac-digest hmac)))

(defun encode-signature (octets url-encode-p)
  (let ((base64 (cl-base64:usb8-array-to-base64-string octets)))
    (if url-encode-p
      (url-encode base64)
      base64)))


(defun build-auth-string (parameters)
  (format nil "OAuth ~{~A=~S~^, ~}"
          (loop for (key value) in parameters
                collect (url-encode key)
                collect (url-encode value))))





;;hijacked from cl-oauth with permission
(defun sort-parameters (parameters)
  "Sort PARAMETERS according to the OAuth spec. This is a destructive operation."
  (sort parameters #'string< :key (lambda (x)
                                    "Sort by key and value."
                                    (concatenate 'string (princ-to-string (car x))
                                                 (princ-to-string (cdr x))))))

(defun oauth1-drakma-request (end-point app-id app-secret 
                              &key  access-token access-secret  
                              callback-uri oauth-verifier 
                              method parameters 
                              content
                              content-type
                              signature-parameters
                              want-stream preserve-uri
                              body-to-string-p
                              parse-json-p)
  (let* ((stamp (format nil "~A" (dx-utils:get-unix-time)))
         (nonce (format nil "~A~A" (random 1234567) stamp))) 

    (let ((final-params (sort-parameters
                         (append  `(,@(if callback-uri
                                          `(("oauth_callback" ,callback-uri)))
                                    ("oauth_consumer_key" ,app-id)
                                    ("oauth_nonce" ,nonce)
                                    ("oauth_signature_method" "HMAC-SHA1")
                                    ("oauth_timestamp" ,stamp)
                                    ,@(if access-token
                                          `(("oauth_token" ,access-token)))
                                    ,@(if oauth-verifier
                                          `(("oauth_verifier" ,oauth-verifier)))
                                    ("oauth_version" "1.0"))
                                  
                                  signature-parameters))))

      
      (drakma-request
       end-point
       :method method 
       :parameters parameters  
       :content content
       :content-type content-type
       :additional-headers
       `(("Authorization"
          ,@(build-auth-string
             `(,@(if callback-uri
                     `(("oauth_callback" ,callback-uri)))
                 ("oauth_consumer_key" ,app-id)
                 ("oauth_nonce" ,nonce)
                 ("oauth_signature"
                  ,(encode-signature
                    (hmac-sha1
                     (signature-base-string
                      :uri end-point 
                      :request-method method
                      :parameters final-params)
                     (hmac-key  app-secret
                                (if access-secret
                                    access-secret
                                    "")))
                    nil))
                 ("oauth_signature_method" "HMAC-SHA1")
                 ("oauth_timestamp" ,stamp)
                 ,@(if access-token
                       `(("oauth_token" ,access-token)))
                 ,@(if oauth-verifier
                       `(("oauth_verifier" ,oauth-verifier)))
                      
                 ("oauth_version" "1.0"))
             )))
       :want-stream want-stream
       :preserve-uri preserve-uri
       :body-to-string-p body-to-string-p
       :parse-json-p parse-json-p))))

(defun alist-to-url-encoded-string (alist external-format)
  (with-output-to-string (out)
    (loop for first = t then nil
          for (name . value) in alist
          unless first do (write-char #\& out)
          do (format out "~A~:[~;=~A~]"
                     (url-encode name external-format)
                     value
                     (url-encode value external-format)))))